<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('produto');
            $table->integer('municipio');
            $table->integer('quantidade')->unsigned();
            $table->integer('ano')->unsigned();
            $table->integer('mes')->unsigned()->default(0);
            $table->string('tipo');

            $table->unique(['produto', 'municipio', 'ano', 'mes']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producao');
    }
}
