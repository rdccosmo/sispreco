#!/bin/bash

cp .env.example .env

php artisan key:generate

php vendor/bin/phpunit --coverage-text --colors=never
